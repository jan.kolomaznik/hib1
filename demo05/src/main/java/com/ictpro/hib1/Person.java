package com.ictpro.hib1;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "id_person")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    @Column(name = "nickname", nullable = false, length = 200)
    private String nickname; // character varying(200) NOT NULL,

    @Column(name = "first_name", nullable = false, length = 200)
    private String firstName; // character varying(200) NOT NULL,

    @Column(name = "last_name", nullable = false, length = 200)
    private String lastName; // character varying(200) NOT NULL,
    

    //@OneToOne()
    //private Name name;

    @ManyToOne
    @JoinColumn(name = "id_location")
    private Location location; // integer,

    @Column(name = "birth_day")
    private LocalDate birthDay; // date,

    private Integer height; // integer,

    @Enumerated(EnumType.STRING)
    private Gender gender; // gender,

    @OneToMany
    @JoinColumn(name = "id_person")
    private Collection<Contact> contacts;
    
    @ManyToMany
    @JoinTable(name = "person_meeting",
            joinColumns = @JoinColumn(name = "id_person"),
            inverseJoinColumns = @JoinColumn(name = "id_meeting")
    )
    private Collection<Meeting> meetings;
    
    protected Person() {
    }

    public Person(String nickname, String firstName, String lastName) {
        this.nickname = nickname;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Collection<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Collection<Contact> contacts) {
        this.contacts = contacts;
    }
    
    

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(nickname, person.nickname) &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(birthDay, person.birthDay) &&
                Objects.equals(height, person.height) &&
                gender == person.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, firstName, lastName, birthDay, height, gender);
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", nickname=" + nickname + ", firstName=" + firstName + ", lastName=" + lastName + ", location=" + location + ", birthDay=" + birthDay + ", height=" + height + ", gender=" + gender + ", meetings=" + meetings.size() + '}';
    }


}
