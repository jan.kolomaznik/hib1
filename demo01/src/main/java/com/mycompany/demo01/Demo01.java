/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.demo01;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.mapping.MetadataSource;
/**
 *
 * @author Student
 */
public class Demo01 {
    
    public static void main(String ... args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        SessionFactory sessionFactory = new MetadataSources(registry)
                .buildMetadata()
                .buildSessionFactory();
        
        try (Session session = sessionFactory.openSession()) {
            
            session.beginTransaction();
            
            Location location = new Location();
            location.setCity("Brno");
            location.setName("Domov");
            location.setCountry("CR");
            System.out.println(session.save(location));
 
            session.getTransaction().commit();
            
        }   
    }
}
