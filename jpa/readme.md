[remark]:<class>(center, middle)
# Java Persistence API


[remark]:<slide>(new)
## Úvod

Java Persistence API poskytuje vývojářům objektově / relační mapování a zařízení pro správu relačních dat v Java aplikacích. 

Jedná se o "nadstavbu" nad **JDBC**. 

Java Persistence se skládá ze čtyř oblastí:

-   Java Persistence API

-   Query language

-   Java Persistence Criteria API

-   Object/relational mapping metadata

[remark]:<slide>(new)
## Entities

Entita je Java POJO objekt s persistencí do databáze. 

Typicky jedna Entita představuje jednu tabulku v relační databázi, a každá instance objektu odpovídá jednomu řádku v této tabulce.

Stav entity je reprezentována buď prostřednictvím atributů objektu, nebo vazbami na jiné entity.

Tato atributy nebo vazby jsou mapovány pomocí anotací.

[remark]:<slide>(new)
### Požadavky na Entitní třídu

- Třída musí být označená s anotací `@Entity`.

- Třída musí mít veřejný (`public`) či chráněné (`protected`) konstruktor bez argumentů, může mít jiné konstruktory.

- Třída nesmí být označena za `final`, stejně tak metody nebo atributy.

- Entity používané v session beans musí implementovat rozhraní `Serializable`.

- Entity mohou být potomky a být děděny ne-entitními.

- Atributy třídy mohou být `private`, `protected` nebo `package-protected`. Klienti musí přistupovat k atributům pouze prostřednictvím metod.

[remark]:<slide>(new)
### Povolené datové typy v Entitní třídě

- Java primitive types

- java.lang.String

- Other serializable types: 
  * Wrappers of Java primitive types,
  * `java.math.BigInteger`, `java.math.BigDecimal`,
  * `java.util.Date`, `java.util.Calendar`, `java.sql.Date`, `java.sql.Time`, `java.sql.TimeStamp`, 
  * User-defined serializable types, 
  * `byte[]`, `Byte[]`, `char[]`, `Character[]`.

- Enumerated types

- Other entities and/or collections of entities

- Embeddable classes

[remark]:<slide>(new)
### Perzistence kolekcí

Entity mohou obsahovat kolekce jiných entit.

Pro kolekce mohou týt použitý následující rozhraní: *java.util.Collection, java.util.Set, java.util.List, java.util.Map*

Pokud kolekce obsahuje primitivní typy, musá výt označena anotací `@ElementCollection` s atibutem `fetch` udává `LAZY` nebo `EAGER`.

```java
@Entity
public class Person {
    ...
    @ElementCollection(fetch=EAGER)
    protected Set<String> nickname = new HashSet<>();
    ...
}
```

[remark]:<slide>(new)
### Validace persistentních atributů 

Pro validaci se používá API JavaBeans Validation.

Pomocí anotací `@PrePersist`, `@PreUpdate` a `@PreRemove` lze určit okamžik validace během životního cyklu Entity.

Například použití annotace `@NotNull`

```java
@Entity
public class Contact implements Serializable {
        
    @NotNull
    protected String name;
    ...
    
```

[remark]:<slide>(wait)
Validace pomocí regulárního výrazu `@Pattern` a validace nastavení datumu.

```java
    @Pattern(regexp = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
            message = "{invalid.phonenumber}")
    protected String mobilePhone;
    
    @Temporal(TemporalType.DATE)
    @Past
    protected Date birthday;
    ...
}
```

[remark]:<slide>(new)
### Primární klíče (jednoduché)

Jednoduché primární klíče se označují anotaci `@Id`.

Jako primární klíč lze použit: 
- celočíselné primitivní typy, 
- Java primitive wrapper types, 
- `java.lang.String`, 
- `java.util.Date`, `java.sql.Date`, 
- `java.math.BigDecimal`, `java.math.BigInteger`
- `java.util.UUID`

U některých typů klíčů lze nastavit strategii pro generování

```java
@Entity
public class Contact implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    ...
}
```

[remark]:<slide>(new)
### Primární klíče (složené)

Složené primární klíče jsou reprezentovány třídou, která:

* Musí být public.
* Gettry a Settry musí mít viditelnost **větší než** `private`.
* Musí mít *default* konstruktor.
* Musí implementovat metody `hashCode()` a `equals(Object other)`.
* Musí implementovat rozhraní `Serializable`.
* Složený klíč musí výt prezentován více atributy.

[remark]:<slide>(new)
### Primární klíče (složené)

```java
public final class LineItemKey 
             implements Serializable {
    private Integer customerOrder;
    private int itemId;

    public LineItemKey() {}

    public LineItemKey(Integer order, int itemId) 
    {
        this.setCustomerOrder(order);
        this.setItemId(itemId);
    }

    /* Getters and setters */
    ...
    
```

[remark]:<slide>(new)
### Primární klíče (složené)

```java
    ...
    
    @Override
    public int hashCode() {
        ...
    }

    @Override
    public boolean equals(Object otherOb) {
        ...
    }

    
}
```

[remark]:<slide>(new)
## Vztahy mezi třídami

Směr vztahu může být obousměrný, nebo jednosměrný.

Vlastnící strana vztahu určuje, jakým způsobem je prováděna aktualizace vztahů v databázi.

U vazeb lze rovněž nastavit kaskádní operaci definovanou ve třídě `javax.persistence.CascadeType`: 

*ALL, DETACH, MERGE, PERSIST, REFRESH, REMOVE* 

```java
@OneToMany(cascade=REMOVE, mappedBy="customer")
private Set<Order> orders = new HashSet();
``` 

[remark]:<slide>(new)
### One-to-one

Každá instance entity se vztahuje k jedné instanci jiného subjektu. 

Například: sklad, v němž každý skladovací zásobník obsahuje jeden ovládací prvek

Používá se anotace `@OneToOne` na odpovídajícím atributu.

[remark]:<slide>(new)
### One-to-many

Instance entita může souviset s více instancí jiných subjektů. 

Například: Prodejní objednávky může mít více řádkových položek.

Používá se anotace `@OneToMany` na odpovídajícím atributu.

[remark]:<slide>(new)
### Many-to-one

Více instancí entity může být vztažena k jedné instanci jiného subjektu. 

V příkladu právě výše, vztah řádkové položky k prodejní objednávce. 

Používá se anotace `@ManyToOne` na odpovídajícím atributu.

[remark]:<slide>(new)
### Many-to-many

Instance entity může být spojena s více instancí jiných entit. 

Například: každý kurz má mnoho studentů, a každý student může trvat několik kurzů.

Používá se anotace `@ManyToMany` na odpovídajícím atributu.

[remark]:<slide>(new)
### Embedded třídy

Embedded třídy jsou označené anotací `@Embeddable`

Serializují se do tabulky *své* entity, takže nevytváří vlastní tabulky.

Definice:
```java
@Embeddable
public class ZipCode {
    String zip;
    String plusFour;
    ...
}
```

[remark]:<slide>(new)
### Embedded třídy

Použití:
```java
@Entity
public class Address {
    @Id
    protected long id
    String street1;
    String street2;
    String city;
    String province;
    @Embedded
    ZipCode zipCode;
    String country;
    ...
}
```

[remark]:<slide>(new)
### Dědičnost

Entity podporují třídní dědičnost, polymorfizmus. 

Entit třídy mohou dědit z non-entitnách tříd, a naopak. Entitní třídy mohou bát i abstraktní.

```java
@Entity
public abstract class Employee {
    @Id
    protected Integer employeeId;
    ...
}
@Entity
public class FullTimeEmployee extends Employee {
    protected Integer salary;
    ...
}
```

[remark]:<slide>(new)
### Dědičnost (strategie mapování)

Dědičnost je možné da relační databáze namapovat třemi způsoby:

1. Strategie *A single table per class hierarchy*
2. Strategie *A table per concrete entity class*
3. Strategie *A "join" strategy*

[remark]:<slide>(new)
## Managament Entit

Entity jsou řízeny správcem, který je reprezentován instancemi `javax.persistence.EntityManager`.

Každá instance EntityManager je spojena s určitým kontextem, který řídí ukládá entit v úložišti.

EntityManager manager je součástí aplikačního kontejneru a je definován pomocí rozhraní API Java Transaction (JTA) transakce.

***Poznámka:**
V rámci entity lze získat instanci EntityManager pomocí anotace `@PersistenceContext`*
```java
@PersistenceContext
EntityManager em;
```

[remark]:<slide>(new)
### Definice připojení k Databázi

Definuje se v souboru `persistence.xml`

```xml
<persistence>
    <persistence-unit name="OrderManagement">
        <description>This unit manages orders and customers.
            It does not rely on any vendor-specific features and can
            therefore be deployed to any persistence provider.
        </description>
        <jta-data-source>jdbc/MyOrderDB</jta-data-source>
        <jar-file>MyOrderApp.jar</jar-file>
        <class>com.widgets.Order</class>
        <class>com.widgets.Customer</class>
    </persistence-unit>
</persistence>
```

[remark]:<slide>(new)
### Práce s EntityManager v aplikaci

K instanci EntityManager přistupujeme prostřednictvím rozhraní `EntityManagerFactory`

```java
@PersistenceUnit
EntityManagerFactory emf;
```

K získání EntityManager použijeme metodu `createEntityManager()`

```java
EntityManager em = emf.createEntityManager();
```

Dále potřebujeme objekt na řízení transakcí:

```java
@Resource
UserTransaction utx;
```

[remark]:<slide>(new)
### Práce s EntityManager v aplikaci

```java
@PersistenceUnit
EntityManagerFactory emf;
EntityManager em;
@Resource
UserTransaction utx;
...
em = emf.createEntityManager();
try {
    utx.begin();
    em.persist(SomeEntity);
    em.merge(AnotherEntity);
    em.remove(ThirdEntity);
    utx.commit();
} catch (Exception e) {
    utx.rollback();
}
```

[remark]:<slide>(new)
Nalezení Entity podle ID

Metoda `entityManager.find()` se používá k vyhledání entity v úložišti pomocí primárního klíče entity:

```java
@PersistenceContext
EntityManager em;

public void enterOrder(int custID, 
                       Order newOrder){
           
    Customer cust = em.find(Customer.class, 
                            custID);
    cust.getOrders().add(newOrder);
    newOrder.setCustomer(cust);
}
```

[remark]:<slide>(new)
### Uložení instance Entity

Nové nebo stávající entity uložíme pomocí metody `entityManager.persist()`

```java
@PersistenceContext
EntityManager em;

public LineItem createLineItem(Order order, 
                               Product product,
                               int quantity) {
    LineItem li = new LineItem(order, 
                               product, 
                               quantity);
    order.getLineItems().add(li);
    em.persist(li);
    return li;
}

```

[remark]:<slide>(new)
### Odstranění instance Entity

Nové nebo stávající entity uložíme pomocí metody `entityManager.remove()`

```java
public void removeOrder(Integer orderId) {
    try {
        Order order = em.find(Order.class, 
                              orderId);
        em.remove(order);
    } ...
```

Rovněž lze vyvolat synchronizace změn v entitě s datovým úložištěm metodou `entityManager.flush()`

[remark]:<slide>(new)
## Generování schéma databáze

Na základě popisu entit je možné si nechat vygenerovat schéma databáze.

Konfigurace se ukládá do souboru `persistence.xml`

```xml
<property name="javax.persistence.schema-generation.database.action"
           value="drop-and-create"/>

```

[remark]:<slide>(new)
## Dotazy

Dotazovací jazyk umožňuje psát univerzální dotazy bez závislosti na konkrétním typu databáze.

Jazyk dotaz používá abstraktní schéma odvozené z entit, včetně jejich vztahů.

Používá  SQL-like syntax výběru objektů nebo hodnot.

Příklady: [Java EE tutorál](https://docs.oracle.com/javaee/7/tutorial/persistence-querylanguage004.htm)

Dva působy zápisu:

* Dynamický přímo v kódu
* Statický pomocí anotací

[remark]:<slide>(new)
### Dynamický dotaz

```java
public List findWithName(String name) {
return em.createQuery(
    "SELECT c FROM Customer c WHERE c.name LIKE :custName")
    .setParameter("custName", name)
    .setMaxResults(10)
    .getResultList();
}
```

[remark]:<slide>(new)
### Statický dotaz

definice:
```java
@NamedQuery(
    name="findAllCustomersWithName",
    query="SELECT c FROM Customer c WHERE c.name LIKE :custName"
)
```

Použití:
```java
@PersistenceContext
public EntityManager em;
...
customers = em.createNamedQuery("findAllCustomersWithName")
    .setParameter("custName", "Smith")
    .getResultList();
```