/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ictpro.hib1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Student
 */
public class Demo04 {

    private static EntityManagerFactory emf;
    
    public static void main(String ... args) {
        try {
            emf = Persistence.createEntityManagerFactory("com.ictpro.hib1");

            //printListOfContact();
            //printListOfContactType();
            //printListOfLocation();
            //printListOfMeeting();
            //printListOfMeetingWithTags();
            //printListOfPerson();
            //printListOfRelation();
            //printListOfRelationType();
            //printListOfRelationByType();
            printListPersonOfMeeting(14L);
        } finally {
            emf.close();
        }
    }

        static void printListOfLocation() {
        System.out.println("-- List of Location --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("select loc from Location as loc")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
        
    static void printListOfLocation(String city) {
        System.out.println("-- List of Location --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("select loc from Location as loc where loc.address.city = :city order by loc.id")
                    .setParameter("city", city)
                    .setFirstResult(2)
                    .setMaxResults(3)
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    static void printListOfPerson() {
        System.out.println("-- List of Person --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from Person p left join fetch p.contacts left join fetch p.location")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    static void printListOfContact() {
        System.out.println("-- List of Contact --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from Contact")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    static void printListOfContactType() {
        System.out.println("-- List of Contact type --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from ContactType")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    static void printListOfMeeting() {
        System.out.println("-- List of Meeting --");          
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from Meeting")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
    
        static void printListOfMeetingWithTags() {
        System.out.println("-- List of Meeting --");
        List<Meeting> meetings = Collections.EMPTY_LIST;
        
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            meetings = em.createQuery("select m from Meeting as m left join fetch m.tags")
                         .getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        
        meetings.forEach(System.out::println);
    }

    static void printListOfRelation() {
        System.out.println("-- List of Relation --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from Relation")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
    
    static void printListOfRelationType() {
        System.out.println("-- List of Relation type --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("from RelationType")
                    .getResultStream()
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    static void printListOfRelationByType() {
        System.out.println("-- List of Relation by type --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createQuery("select r.id from Relation as r left join fetch r.type as t where t.name = :typeName", Relation.class)
                    .setParameter("typeName", "colleague")
                    .getResultStream()
                    .forEach(System.out::println);
            
            em.createQuery("select t, count(r) from Relation as r join r.type as t group by t", Object[].class)  
                    .getResultStream()
                    .map(Arrays::toString)
                    .forEach(System.out::println);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
    
    
    static void printListPersonOfMeeting(Long idMeeting) {
        System.out.println("-- List of operson on meeting --");
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.find(Meeting.class, idMeeting)
                    .getPersons()
                    .stream()
                    .map(p -> p.getFirstName() + " " + p.getLastName())
                    .forEach(System.out::println);
            
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}
