package com.ictpro.hib1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contact_type")
public class ContactType {

    @Id
    @Column(name = "id_contact_type")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    @Column(name = "name", nullable = false, length = 200)
    private String name; // character varying(200) NOT NULL,

    @Column(name = "validation_regexp", length = 200)
    private String validationRegexp; // character varying(200)

    protected ContactType() {
    }

    public ContactType(String name, String validationRegexp) {
        this.name = name;
        this.validationRegexp = validationRegexp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidationRegexp() {
        return validationRegexp;
    }

    public void setValidationRegexp(String validationRegexp) {
        this.validationRegexp = validationRegexp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactType that = (ContactType) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(validationRegexp, that.validationRegexp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, validationRegexp);
    }

    @Override
    public String toString() {
        return "ContactType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", validationRegexp='" + validationRegexp + '\'' +
                '}';
    }
}
