package com.ictpro.hib1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @Column(name = "id_contact")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // integer NOT NULL,

    //@Column(name = "id_person", nullable = false)
    private Long person; // integer NOT NULL,

    @ManyToOne
    @JoinColumn(name = "id_contact_type", nullable = false)
    private ContactType type; // integer NOT NULL,

    @Column(name = "contact", nullable = false, length = 200)
    private String contact; // character varying(200) NOT NULL

    protected Contact() {
    }

    public Contact(Long person, ContactType type, String contact) {
        this.person = person;
        this.type = type;
        this.contact = contact;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPerson() {
        return person;
    }

    public void setPerson(Long person) {
        this.person = person;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType contactType) {
        this.type = contactType;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact1 = (Contact) o;
        return Objects.equals(person, contact1.person) &&
                Objects.equals(type, contact1.type) &&
                Objects.equals(contact, contact1.contact);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, type, contact);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", person=" + person +
                ", contact='" + contact + '\'' +
                '}';
    }
}
